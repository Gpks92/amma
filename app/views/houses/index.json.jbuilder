json.array!(@houses) do |house|
  json.extract! house, :id, :houseNo, :houseName, :street1, :street2, :city, :state, :pincode
  json.url house_url(house, format: :json)
end
