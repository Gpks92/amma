json.array!(@members) do |member|
  json.extract! member, :id, :firstName, :lastName, :DOB, :age, :relationType, :education, :profession, :mobile, :email, :house_id, :vaniyanType, :tharavad, :perneBenefit
  json.url member_url(member, format: :json)
end
