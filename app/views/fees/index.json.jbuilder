json.array!(@fees) do |fee|
  json.extract! fee, :id, :houseid, :amount, :last_paid_date, :total_amount, :pending_amount
  json.url fee_url(fee, format: :json)
end
