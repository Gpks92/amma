class Fee < ActiveRecord::Base
	belongs_to :house
	#after_create :generate_fee_number
	after_create :toatl_amount_fee

	def generate_fee_number
		self.payment_number=self.house.houseNo.to_s + "_" + self.id.to_s
		self.save
	end

	def toatl_amount_fee
		self.total_amount=self.total_amount.to_i + self.amount.to_i
		self.save
	end

end
