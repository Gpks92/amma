class CreateHouses < ActiveRecord::Migration
  def change
    create_table :houses do |t|
      t.string :houseNo
      t.string :houseName
      t.string :street1
      t.string :street2
      t.string :city
      t.string :state
      t.integer :pincode

      t.timestamps
    end
  end
end
