class CreateFees < ActiveRecord::Migration
  def change
    create_table :fees do |t|
      t.integer :houseid
      t.integer :amount
      t.date :last_paid_date
      t.integer :total_amount
      t.integer :pending_amount

      t.timestamps
    end
  end
end
