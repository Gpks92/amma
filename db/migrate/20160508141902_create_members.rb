class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :firstName
      t.string :lastName
      t.string :DOB
      t.integer :age
      t.string :relationType
      t.string :education
      t.string :profession
      t.string :mobile
      t.string :email
      t.string :houseNo
      t.string :vaniyanType
      t.string :tharavad
      t.string :perneBenefit

      t.timestamps
    end
  end
end
